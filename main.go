package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/glennzw/maltegogo"
)

func gobuster(Url string, File string) string {

	// make this as channels so it pops the findings as they come instead of having to
	// wait until is over to show.

	cmd := exec.Command("gobuster", "-w", File, "-u", Url, "-e", "-n", "-q")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}

	result := fmt.Sprintf("%s", out)
	// todo: check if result contains error from wildcard return
	// todo: Invalid certificate

	return result
}

func main() {
	lt := maltegogo.ParseLocalArguments(os.Args)
	Url := lt.Value
	File := "common.txt" //on maltego make sure you make the transform workingdir to same dir or put whole path

	Dirs := gobuster(Url, File)

	TRX := maltegogo.MaltegoTransform{}
	line := bufio.NewScanner(strings.NewReader(Dirs))

	for line.Scan() {
		dir := line.Text()

		NewEnt := TRX.AddEntity("rek2.Path", "Scanning... "+dir)
		NewEnt.SetType("rek2.Path")
		NewEnt.SetValue(dir)
		//NewEnt.AddDisplayInformation(dir)
		NewEnt.AddProperty("properties.path", "Path", "stict", dir)
		NewEnt.SetLinkColor("#FF0000")
		NewEnt.SetWeight(200)
		//NewEnt.SetNote("Service: " + service)

		TRX.AddUIMessage("completed!", "Inform")
	}
	fmt.Println(TRX.ReturnOutput())
}
