# maltego-gobuster

A "gobuster" transformer to add to our hacking tools/options. and just for fun.
works but still adding features as needed. to change  the file with possible dirs or file names
as of now it has to be in the code before compiling.

![Alt text](/maltego-gobuster.png?raw=true "Maltego with gobuster transform in action")


---

_what do you need?_

Obviosly:
* GoBuster
* Maltego
* Go
---

### Instalation

```
git clone https://gitlab.com/rek2/maltego-gobuster.git
cd maltego-gobuster
go mod init https://gitlab.com/rek2/maltego-gobuster.git
go build -o maltego-gobuster
cp maltego-gobuster ~/bin/maltego-gobuster #or anyplace you want this binary to be
```

- Add the OCR-A_char_Solidus.svg to Maltego
- Open Maltego and IMPORT the path-identity-to-import.mtz
- Open Maltego and add the transform, see below for a link to documentation

- Remember where you put the maltego-gobuster resulting binary you will need that path/localtion for adding a local transform to maltego
- You adding to maltego a binary not a script so will be someththing line "/home/myuser/bin/maltego-gobuster" instead of a scripting language interpreter.

[Upstream instructions in how to install a GO transform, but read my comment above!!!i](https://github.com/NoobieDog/maltegolocal/blob/master/README.md)